import { applyMiddleware, createStore, compose } from 'redux'

import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'

import reducer from './reducers'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const middleware = applyMiddleware(
    // promise(),
    thunk
)

const store = createStore(reducer, composeEnhancers(
    middleware
))

export default store
