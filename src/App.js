import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'

import Home from './components/Home'
import About from './components/About'

import {connect} from 'react-redux'
import * as Actions from './actions'


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <div className="App-header">
            <h2>📝 📇 Currency convertor</h2>
            <Link to="/">Home</Link>&nbsp;
            <Link to="/about">About</Link>
          </div>
          <Switch>
            <Route path="/" exact component={Home} />
            {/* <Route {...this.props}
                path="/"
                exact
                render={<Home {...this.props} />}
            /> */}
            <Route path="/about" component={About} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    rates: state
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () => dispatch(Actions.fetchRates())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
