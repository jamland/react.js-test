const API = 'http://api.fixer.io/latest'

export const fetchRates = () => {
  const request = fetch(API)

  return (dispatch) => {
    request.then((response) => {
      if (!response.ok) {
          throw Error(response.statusText);
      }
      return response;
    }).then((response) => response.json())
      .then((items) => dispatch({
        type: 'FETCHED_RATES_FULFILLED',
        payload: items
      }))
  }
}
