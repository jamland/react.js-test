export default function reducer ( state={
  rates: {},
  fetching: false,
  fetched: false,
  error: null
}, action) {
  switch (action.type) {
    case "FETCHED_RATES_FULFILLED":
      return {
        ...state,
        fetching: false,
        fetched: true,
        rates: {
          "EUR": 1,
          ...action.payload.rates
        }
      }
      break;
    default:
      return state
  }
}
