import React from 'react';

const About = (props) => {
  return (
    <div className="big-text">
      🙀 <br />
      <a href="ITC_TEST_REACT.pdf">Based on this<br /> test task</a>
    </div>
  );
}

export default About
