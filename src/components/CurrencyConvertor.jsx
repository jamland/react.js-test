import React from 'react'

export const CurrencyConvertor = (props) => {
  const options = Object.keys(props.rates).map( (rate) => {
    return (
      <option key={`key-${rate}`}>{rate}</option>
    )
  })

  return (
    <div>
      <select
        name={props.name} 
        type="select"
        value={props.selectedCurrency}
        onChange={props.onInputChange}>
        {options}
      </select>
      <input type="number"
        name={props.name}
        onChange={props.onInputChange}
        value={props.value}
        placeholder={props.placeholder}
        disabled={props.disabled} />
    </div>
  )
}
