import React, {Component} from 'react';

import {CurrencyConvertor} from './CurrencyConvertor'

import {connect} from 'react-redux'
import * as Actions from '../actions'

// TODO  Only accepts "float" numbers
class Home extends Component {
  constructor (props) {
    super(props)

    this.state = {
      'starting_number': null,
      'starting_select-one': 'USD',
      'target_number': null,
      'target_select-one': 'EUR'
    }

    this.inputChangeHandler = this.inputChangeHandler.bind(this)
    this.selectChangeHandler = this.selectChangeHandler.bind(this)
  }

  componentDidMount () {
    this.props.fetchData()
    this.calculateRates()
  }

  inputChangeHandler (event) {
    const type = event.target.type
    const name = event.target.name
    const value = event.target.value

    this.setState({
      [`${name}_${type}`]: value
    }, () => {
      this.calculateRates()
    })
  }

  calculateRates () {
    const startCurrency = this.state['starting_select-one']
    const targetCurrency = this.state['target_select-one']

    const startRate = +this.props.rates[startCurrency]
    const targetRate = +this.props.rates[targetCurrency]

    const startAmount = +this.state.starting_number
    const targetAmount = (startAmount / startRate * targetRate).toFixed(4)

    this.setState({
      target_number: targetAmount
    })
  }

  selectChangeHandler (event) {
    this.setState({
      [event.target.name+'-select']: event.target.value
    })
  }

  render () {
    return (
      <div className="big-text">
        <CurrencyConvertor
          selectedCurrency={this.state['starting_select-one']}
          name="starting"
          placeholder="Starting amount..."
          value={this.state.starting_number}
          onInputChange={this.inputChangeHandler}
          rates={this.props.rates}
        />
        <CurrencyConvertor
          selectedCurrency={this.state['target_select-one']}
          name="target"
          placeholder="Target amount..."
          value={this.state.target_number}
          onInputChange={this.inputChangeHandler}
          rates={this.props.rates}
          disabled="disabled"
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    rates: state.rates,
    error: state.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () => dispatch( Actions.fetchRates() )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
